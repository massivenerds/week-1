'use strict';

module.exports = {
    highcard: [1, 1, 1, 1, 1],
    onepair: [2, 1, 1, 1],
    twopairs: [2, 2, 1],
    threeofakind: [3, 1, 1],
    fullhouse: [3, 2],
    fourofakind: [4, 1]
};