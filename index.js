'use strict';

var fs = require('fs');
var path = require('path');
var readline = require('readline');
var pokerhands = require('./pokerhands');
var frequencies = require('./frequencies');
var ranks = require('./ranks');
var _ = require('lodash');

var rl = readline.createInterface({
    input: fs.createReadStream(path.join(__dirname, 'p054_poker.txt'))
});

var results = {
    wins: 0
};

//Execute on each line
rl.on('line', (input) => {

    var deal = input.split(' ');
    var players = [
        {
            name: 'Player 1',
            hand: sortHand(deal.splice(0, 5)),
            histogram: {}
        },
        {
            name: 'Player 2',
            hand: sortHand(deal),
            histogram: {}
        }
    ];

    for (let player of players) {
        inspectHand(player);
    }

    determineWin(players);
});

//Execute at end of file
rl.on('close', () => {
    console.log(`Player One wins ${results.wins} hands.`);
});

var inspectHand = (player) => {

    //create histogram object
    player.hand.forEach((card) => {
        player.histogram[card.rank] = player.histogram[card.rank] ? player.histogram[card.rank] + 1 : 1;
    });

    //convert histogram to array for sort
    player.histogram = Object.keys(player.histogram).map(function (key) {
        return {
            rank: key,
            frequency: player.histogram[key]
        };
    }).sort((a, b) => {
        //sort by frequency
        if (b.frequency > a.frequency) {
            return 1;
        } else if (b.frequency < a.frequency) {
            return -1;
        }

        //then sort by rank
        return compareRanks(a.rank, b.rank);
    });

    //compare card frequencies to map and set initial pokerhand
    player.pokerhand = pokerhands[_.findKey(frequencies, (item) => {
        return _.isEqual(item, player.histogram.map((x) => x.frequency));
    })];

    //check for flush
    var isFlush = !!player.hand.map((card) => card.suit).reduce((a, b) => {
        return (a === b) ? a : NaN;
    });

    //check for straight
    var isStraight = (player.histogram.length === 5) && ((player.hand[0].value - player.hand[4].value === 4) || (player.hand[0].value === 14 && player.hand[1].value === 5));

    var straightOrFlushHand = (isStraight && isFlush) ? pokerhands['straightflush'] :
        isStraight ? pokerhands['straight'] :
            isFlush ? pokerhands['flush'] : {value: -1};

    //if hand has a more valuable pokerhand, switch it
    if (straightOrFlushHand.value > player.pokerhand.value) {
        player.pokerhand = straightOrFlushHand;
    }

};

var sortHand = (context) => {
    return context.map((card) => {
        var markings = card.split('');
        return {
            rank: markings[0],
            suit: markings[1],
            value: ranks[markings[0]]
        };
    }).sort((a, b) => {
        return compareRanks(a.rank, b.rank);
    });
};

var compareRanks = (a, b) => {
    var order = '23456789TJQKA';
    if (order.indexOf(b) < order.indexOf(a)) {
        return -1;
    } else if (order.indexOf(b) > order.indexOf(a)) {
        return 1;
    } else {
        return 0;
    }
};

var determineWin = (players) => {
    if (players[0].pokerhand.value > players[1].pokerhand.value) {
        results.wins++;
    } else if (players[0].pokerhand.value === players[1].pokerhand.value) {
        players[0].histogram.some((card, index) => {

            if (ranks[card.rank] > ranks[players[1].histogram[index].rank]) {
                results.wins++;
                return true;
            } else if (ranks[card.rank] < ranks[players[1].histogram[index].rank]) {
                return true;
            }

            return false;
        });
    }
};