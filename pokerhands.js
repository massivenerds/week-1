'use strict';

module.exports = {
    highcard: {
        name: 'High Card',
        value: 1
    },
    onepair: {
        name: 'One Pair',
        value: 2
    },
    twopairs: {
        name: 'Two Pairs',
        value: 3
    },
    threeofakind: {
        name: 'Three of a Kind',
        value: 4
    },
    straight: {
        name: 'Straight',
        value: 5
    },
    flush: {
        name: 'Flush',
        value: 6
    },
    fullhouse: {
        name: 'Full House',
        value: 7
    },
    fourofakind: {
        name: 'Four of a Kind',
        value: 8
    },
    straightflush: {
        name: 'Straight Flush',
        value: 9
    },
    royalflush: {
        name: 'Royal Flush',
        value: 10
    }
};